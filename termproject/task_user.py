''' @file                       task_user.py
    @brief                      A task for interfacing with the ball balancing platform.
    @details                    Contains the class Task_User.
                                See state diagram below.
                                \image html FinalusertaskFSM.jpg
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @author                     Kendall Chappell
    @date                       December 4, 2021
'''

# placeholder for when no command needs to be run
_NO_COMMAND = 0

# Using IMU instead
# # Counts per revolution of our encoders
# CPR = 4000

import pyb
import utime
#import math


class Task_User:
    ''' @brief                  A task for user interaction with the ball balancing platform.
        @details                This class is a task for taking user input and
                                controlling a ball balancing platform display program.
                                It uses shares to send commands to the controller and 
                                data collector.
    '''
    def __init__(self, periodCheckCommands, collectCMD, balanceCMD):
        '''@brief       Initialize a user task
           @details     This will instantiate an User object which interfaces 
                        with the ball balancing platform. When user enters
                        commands. they are shared with the appropriate tasks
                        
           @param       periodCheckCommands     Period, in microseconds, at which 
                                                to check for new user commands.
           @param       collectCMD              Share indicating if the 
                                                data_collect_task should run. 
           @param       balanceCMD              Share indicating if the 
                                                controller_task should run.
           
        '''
        self._serport = pyb.USB_VCP()
        self._periodCheckCommands = periodCheckCommands
        self._nextTimeCheckCommands = utime.ticks_add(utime.ticks_us(), self._periodCheckCommands)
        
        self._collectCMD = collectCMD
        self._balanceCMD = balanceCMD
        
        # for state and command control
        self._command = _NO_COMMAND
#        self.state = CALIBRATE
        
#        # for collecting user inputs
#        self.numString = ""
    
    
    def run(self):
        '''@brief       Run the user task.
           @details     Uses a finite state machine to interact with the user.
                        Commands are entered through characters at the serial
                        port. See list_commands() for command options.
                        See state diagram below.
                        \image html FinaltaskuserFSM.jpg
        '''

# State for Regular Running
#        if self.state == WAIT:        
            
        # check if command
        if utime.ticks_diff(utime.ticks_us(), self._nextTimeCheckCommands) >= 0:
            self._nextTimeCheckCommands = utime.ticks_add(self._nextTimeCheckCommands, self._periodCheckCommands)
            
            if self._serport.any():
                self._command = self._serport.read(1)
    
            if self._command == b'b' or self._command == b'B':
                self._balanceCMD.write(True)
                print('Balancing Ball ...')
                self._command = _NO_COMMAND
            elif self._command == b'c' or self._command == b'C':
                self._collectCMD.write(True)
                print('Collecting Data ...')
                self._command = _NO_COMMAND
            elif self._command == b's' or self._command == b'S':
                self._balanceCMD.write(False)
                print('Stoping Balancing Ball')
                self._command = _NO_COMMAND
            elif self._command == b'e' or self._command == b'E':
                self._collectCMD.write(False)
                print('Ending Collecting Data')
                self._command = _NO_COMMAND
  
    def list_commands(self):
        '''@brief       List the user commands available.
           @details     The available commands are:
                            - 'b' or 'B': balance ball
                            - 'c' or 'C': collect run data
                            - 's' or 'S': stop balancing ball
                            - 'e' or 'E': end data collection
        '''
        print("The available commands are:")
        print("    'b' or 'B': balance ball")
        print("    'c' or 'C': collect run data")
        print("    's' or 'S': stop balancing ball")
        print("    'e' or 'E': end data collection")
        
    
# # State for collecting user input
#        if self.state == NUM_INPUT:
#            
#            # Check if character is valid, then append to Number String
#            if self.serport.any():
#                nextChar = self.serport.read(1).decode()
#                if nextChar.isdigit():
#                    self.numString += nextChar
#                    print('\r', self.numString, sep='', end='')
#                elif nextChar == '-':
#                    if self.numString == '':
#                        self.numString += nextChar
#                        print('\r', self.numString, sep='', end='')
#                elif nextChar == '.':
#                    if '.' not in self.numString:
#                        self.numString += nextChar
#                        print('\r', self.numString, sep='', end='')
#                elif nextChar == '\x7F':
#                    if self.numString != '':
#                        self.numString = self.numString[:-1]
#                        print('\r', self.numString, ' ', '\x7f', sep='', end='')
#                        
#                # when the user pressess enter, the number is assigned to correct variable
#                elif (nextChar == '\r' or nextChar == '\n') and self.numString != '':
#                    print()
#                    if self.inputID == C_DUTY1 or self.inputID == C_DUTY2:
#                        self.duty = float(self.numString)
#                    elif self.inputID == C_KP1 or self.inputID == C_KP2:
#                        self.Kp = float(self.numString)
#                    elif self.inputID == C_OMEGA1 or self.inputID == C_OMEGA2:
#                        self.omega = float(self.numString)
#                    self.state = WAIT
#                    self.numString = ''                   



    