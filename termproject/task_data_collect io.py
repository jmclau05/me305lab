''' @file                       task_data_collect.py
    @brief                      A task for updating an encoder
    @details                    Contains the class Task_User.
                                See state diagram below.
                                \image html Lab3usertaskFSM.jpg
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       December 4, 2021
''' 

import os
import pyb
import utime
import io
import string
import micropython
# constants for positions of data in tuples
X = 0
XDOT = 1
THETA_Y = 0
THETA_YDOT = 1

Y = 2
YDOT = 3
THETA_X = 2
THETA_XDOT = 3

# States
_WAIT = micropython.const(0)
_COLLECT = micropython.const(1)
_WRITE = micropython.const(2)
_GET_FILENAME = micropython.const(3)


class Task_Data_Collect:
    ''' @brief      
        @details    
    '''
    
    def __init__(self, COLLECT_PERIOD, collectCMD, imu_share, panel_share):
        ''' @brief      
            @details    
            @param      IMU_PERIOD
            @param      imu_share
        '''
        self.periodCollect = COLLECT_PERIOD
        self.nextTimeCollect = utime.ticks_add(utime.ticks_us(), self.periodCollect)
        
        self.imu_share = imu_share
        self.panel_share = panel_share
        
        self.collectCMD = collectCMD

        self._buf = ""
        self._lines = 0
        self._start_time = None

        self._state = _WAIT
        self._filename = ""

    def run(self):
        '''@brief       Run the imu task.
           @details     
        '''
        if utime.ticks_diff(utime.ticks_us(), self.nextTimeCollect) >= 0:
            self.nextTimeCollect = utime.ticks_add(self.nextTimeCollect, self.periodCollect)

            if self._state == _WAIT:
                if self.collectCMD.read():
                    self._state == _COLLECT
            
            if self._state == _COLLECT:
                if self.collectCMD.read():
                    if self._start_time == None:
                        self._start_time = utime.ticks_us()
                    currentTime = utime.ticks_diff(utime.ticks_us, self.startTime)
                    thy, thyd, thx, thxd = self.imu_share.read()
                    x, y, xd, yd = self.panel_share.read()
                    data_string = f"{currentTime},,{x},{thy},{xd},{thyd},,{y},{thx},{yd},{thxd}\n"
                    self.buf += data_string
                    if self._lines >= self._buf_length_lines:
                        if self._filename == None:
                            self.state = _GET_FILENAME
                        else:
                            self.state = _WRITE
                else:
                    if self.buf == "":
                        self._state = _WAIT
                    else:
                        if self._filename == "":
                            self._state = _GET_FILENAME
                        else:
                            self._state = _WRITE
            
            if self._state == _GET_FILENAME:
                data_nums = []
                for file in os.listdir():
                    if file[0:4] == "data":
                        data_nums.append(int(file[4:7]))
                self._filename = f"data{max(data_nums):03}.csv"
                self._state = _WRITE

            if self.state == _WRITE:
                with open(self.filename, 'a') as f:
                    f.write(self._buf)
                self._buf = ""
                self._lines = 0
                if not self.collectCMD.read():
                    self._filename = ""
                self._state = _WAIT
        
        # if self.collectCMD.read():
        #     # Check if time to collect data
        
        
        #         # Read data from shares
        #         thy, thyd, thx, thxd = self.imu_share.read()
        #         x, y, xd, yd = self.panel_share.read()
                
        #         # Record Start Time if collectCMD just became true
        #         if self.startTime == None:
        #             self.startTime = utime.ticks_us()
        #         currentTime = utime.ticks_diff(utime.ticks_us, self.startTime)
                
        #         # load data into buffer
        #         self.buf += f"{currentTime},,{x},{thy},{xd},{thyd},,{y},{thx},{yd},{thxd}"
        #         self.lines += 1

        #         # Check if time to write
        #         if lines == self.lines_to_write:
        #             # Get filename if none
        #             if self.filename == None:
        #                 for file in os.listdir():
        #                     if file[0:4] == "data":
        #                         data_nums.append()
        # else:
        #     self.startTime = None

             

        
        
#             if utime.ticks_diff(utime.ticks_us(), self.nexTimeGetData) >= 0:
#                
#                 if self.collectID == 1:
#                     data = self.dataShare1.read()
#                 elif self.collectID == 2:
#                     data = self.dataShare2.read()
#                 else:
#                     raise ValueError("Invalid collect ID. Collect ID should be 1 or 2")
#                
        #         t_ticks = data[TIME]
                
        #         pos_rad = data[POSITION] / CPR * 2 * math.pi
                
        #         if self.dataQueue.num_in() == 0:    
        #             delta_rad_s = 0
        #             self.last_pos = pos_rad
        #             self.last_time = t_ticks  # not used first loop
        #         else:
        #             delta_rad_s = (pos_rad - self.last_pos) / (utime.ticks_diff(t_ticks, self.last_time) / 1000000)
        #             self.last_pos = pos_rad
        #             self.last_time = t_ticks  # update for next round
                
        #         self.dataQueue.put((t_ticks, pos_rad, delta_rad_s))
        #         if self.dataQueue.num_in() == self.dataLength:
        #             self.state = PRINT_DATA
                
        #         self.nexTimeGetData = utime.ticks_add(self.nexTimeGetData, self.periodGetData)
        
        
        
        
        