''' @file                       task_data_collect.py
    @brief                      A task for updating an encoder
    @details                    Contains the class Task_User.
                                See state diagram below.
                                \image html Lab3usertaskFSM.jpg
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       December 4, 2021
''' 

import os
import pyb
import utime
import io
import string
import micropython
# constants for positions of data in tuples
X = 0
XDOT = 1
THETA_Y = 0
THETA_YDOT = 1

Y = 2
YDOT = 3
THETA_X = 2
THETA_XDOT = 3


class Task_Data_Collect:
    ''' @brief      
        @details    
    '''
    
    def __init__(self, COLLECT_PERIOD, collectCMD, imu_share, panel_share):
        ''' @brief      
            @details    
            @param      IMU_PERIOD
            @param      imu_share
        '''
        self.periodCollect = COLLECT_PERIOD
        self.nextTimeCollect = utime.ticks_add(utime.ticks_us(), self.periodCollect)
        
        self.imu_share = imu_share
        self.panel_share = panel_share
        
        self.collectCMD = collectCMD

        self._start_time = None


    def run(self):
        '''@brief       Run the imu task.
           @details     
        '''
        if utime.ticks_diff(utime.ticks_us(), self.nextTimeCollect) >= 0:
            self.nextTimeCollect = utime.ticks_add(self.nextTimeCollect, self.periodCollect)
            if self._start_time == None:
                self._start_time = utime.ticks_us()
            currentTime = utime.ticks_diff(utime.ticks_us, self.startTime)
            thy, thyd, thx, thxd = self.imu_share.read()
            x, y, xd, yd = self.panel_share.read()
            print(f"{currentTime},,{x},{thy},{xd},{thyd},,{y},{thx},{yd},{thxd}\n")