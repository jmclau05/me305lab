''' @file       BNO055.py
    @brief      A driver for the BNO055 IMU from Bosch.
    @details    Contains the class BNO055 
    @author     Tori Bornino
    @author     Jackson McLaughlin
    @author     Kendall Chappell
    @date       December 4, 2021
'''

# Operating Modes
_CONFIGMODE = 0b0000
_NDOF = 0b1100
# Regester Addresses
 # Device I2C IMU Address
_IMU_ADDR = 0x28
 # Operation Mode
_OPR_MODE = 0x3D
 # Calibration Status
_CALIB_STAT = 0x35
 # Calibration Coefficients
_CALIB_START = 0x55  # to 6A
_CALIB_LENGTH = 22
 # Euler Angle Data
_EUL_START = 0x1A  # to 1F
_EUL_LENGTH = 6
#EUL_Pitch_MSB = 0x1F
#EUL_Pitch_LSB = 0x1E
#EUL_Roll_MSB = 0x1D
#EUL_Roll_LSB = 0x1C
#EUL_Heading_MSB = 0x1B
#EUL_Heading_LSB = 0x1A
 # Gyroscope Angular Acceleration Data
_GYR_START = 0x14  # to 19
_GYR_LENGTH = 6
#GYR_DATA_Z_MSB = 0x19
#GYR_DATA_Z_LSB = 0x18
#GYR_DATA_Y_MSB = 0x17
#GYR_DATA_Y_LSB = 0x16
#GYR_DATA_X_MSB = 0x15
#GYR_DATA_X_LSB = 0x14

import ustruct

class BNO055:
    ''' @brief      IMU class for Bosch BNO055
        @details    Allows a BNO055 IMU to have its operating mode set or read,
                    calibation status read, calibration to be done, euler
                    angles to be read (in degrees heading, pitch, and roll),
                    and angular velocities to be read (degrees/s x, y and z).
    '''
    
    def __init__(self, i2cController):
        ''' @brief      Initializes an IMU object.
            @details    Takes in an I2C controller object to use for
                        communicating with the IMU.
            @param      i2cController   The I2C controller object.
        '''
        self._i2cController = i2cController
    
    def set_operating_mode(self, oprMode):
        ''' @brief      Sets the BNO055's operating mode.
            @details    Sets the OPR_MODE register on the BNO055.
                        See Table 3-3 in the [BNO055 datasheet]
                        (https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf)
                        for a list of operating modes.
                        Currently, we only use the CONFIGMODE and NDOF modes
                        (see variables at top of BNO055.py)
            @param      oprMode     The operating mode to set.
        '''
        self._i2cController.mem_write(oprMode, _IMU_ADDR, _OPR_MODE)
    
    def get_calibration_status(self):
        ''' @brief      Gets the IMU's calibration status.
            @details    Returns the value stored in the CALIB_STAT register on
                        the BNO055. See Table 4-2: Register Map in the [BNO055 datasheet]
                        (https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf)
                        for the meaning of the bits in the status.
            @return     The calibration status (byte).
        '''
        return self._i2cController.mem_read(1, _IMU_ADDR, _CALIB_STAT)
    
    def get_calibration_coefficients(self):
        ''' @brief      Gets the current calibration coefficents of the IMU.
            @details    Returns the values stored in registers 0x35 to 0x6A
                        on the BNO055. See Table 4-2: Register Map in the [BNO055 datasheet]
                        (https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf)
                        for the map of the coefficients returned.
            @return     Calibration Coefficents (22 bytes)
        '''
        return self._i2cController.mem_read(_CALIB_LENGTH, _IMU_ADDR, _CALIB_START)
    
    def set_calibration_coefficients(self, calibCoeff):
        ''' @brief      Sets the calibration coefficients of the IMU.
            @details    Sets the values stored in registers 0x35 to 0x6A
                        on the BNO055. See Table 4-2: Register Map in the [BNO055 datasheet]
                        (https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf)
                        for the map of the coefficients.
        '''
        self._i2cController.mem_write(calibCoeff, _IMU_ADDR, _CALIB_START)
    
    def read_euler_angles(self):
        ''' @brief      Reads the current euler angles (degrees).
            @details    Returns a tuple of (heading, pitch, roll) in degrees.
            @return     Euler angles (degrees) as a tuple (heading, pitch, roll).
        '''
        angles = self._i2cController.mem_read(_EUL_LENGTH, _IMU_ADDR, _EUL_START)
        (heading, pitch, roll) = ustruct.unpack('hhh', angles)
        eulerGain = 16
        return heading / eulerGain, pitch / eulerGain, roll / eulerGain # degrees
        
    def read_angular_velocity(self):
        ''' @brief      Reads the current angular velocity (deg/s).
            @details    Returns a tuple of (x, y, z) angular velocities (deg/s).
            @return     Angular velocities.
        '''
        
        vels = self._i2cController.mem_read(_GYR_LENGTH, _IMU_ADDR, _GYR_START)
        (x, y, z) = ustruct.unpack('hhh', vels)
        omegaGain = 16
        return x / omegaGain, y / omegaGain, z / omegaGain # deg/s
    

