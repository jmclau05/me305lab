''' @file                       task_data_collect.py
    @brief                      A task for printing collected data to the console.
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @author                     Kendall Chappell
    @date                       December 4, 2021
''' 

import utime

class Task_Data_Collect:
    ''' @brief                  A task for printing collected data to the console
        @details                This class is a task for reading the ball balancing 
                                platform run data from shares and printing it to 
                                the console.

    '''
    
    def __init__(self, COLLECT_PERIOD, collectCMD, imu_share, panel_share, motor_share):
        '''@brief       Initialize a data collection task
           @details     This will instantiate a data collection object which 
                        collects data from the ball balancing platform and, 
                        when enabled, prints to the console.                        
           @param       periodCollect   Period, in microseconds, at which 
                                        to print data.
           @param       collectCMD      Share indicating if the 
                                        data_collect_task should print data.
           @param       imu_share       Share containing data collected by the imu
                                        stored in a tuple 
                                        (theta_y, theta_ydot, theta_x, theta_xdot)
                                        with units (deg, deg/s, deg, deg/s)
           @param       panel_share     Share containing data collected by the 
                                        resistive touch panel stored in a tuple 
                                        (x, xdot, y, ydot)
                                        with units (mm, mm/, mm, mm/s).
           @param       motor_share     Share containing the motor percent duty cycle  
                                        as a tuple (DutyX, DutyY).
        '''
        self._periodCollect = COLLECT_PERIOD
        self._nextTimeCollect = utime.ticks_add(utime.ticks_us(), self._periodCollect)
        
        self._imu_share = imu_share
        self._panel_share = panel_share
        
        self._collectCMD = collectCMD

        self._start_time = None
        self._motor_share = motor_share


    def run(self):
        '''@brief       Run the data collect task.
           @details     If the collect command returns True, then data is read 
                        from the imu, panel, and motor shares and printed to 
                        the console. 
        '''
        if self._collectCMD.read():
            
            if utime.ticks_diff(utime.ticks_us(), self._nextTimeCollect) >= 0:
                self._nextTimeCollect = utime.ticks_add(self._nextTimeCollect, self._periodCollect)
                if self._start_time == None:
                    self._start_time = utime.ticks_us()
                currentTime = utime.ticks_diff(utime.ticks_us(), self._start_time) / 1e6
                thy, thyd, thx, thxd = self._imu_share.read()
                x, xd, y, yd, z = self._panel_share.read()
                duty_x, duty_y = self._motor_share.read()
                print(f"{currentTime},,{x},{thy},{xd},{thyd},{duty_x},,{y},{thx},{yd},{thxd},{duty_y}")
        else:
            self._start_time = None
                
                
                
                
                
                