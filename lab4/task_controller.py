''' @file                       task_controller.py
    @brief                      A task for running a control loop
    @details                    Contains the class Task_Controller.
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       October 26, 2021
'''
import utime

class Task_Controller:
    ''' @brief      A task for setting a motor's duty cycle.
        @details    Reads a motor's duty cycle from a share and sets it. 

                    
    '''
    
    def __init__(self, period, closedLoop):
        ''' @brief      Initializes a motor task.
            @details    Sets up a motor to read its duty cycle from a share and
                        set it, periodically.
            @param      motorID     The ID of the motor to use (1 or 2).
            @param      motorShare  The share holding the duty cycle values to be set.
            @param      period      The period, in microseconds, to run this task.
            @param      motor_drv   The motor driver object.
        '''
        self.period = period
        self.nextTime = utime.ticks_add(utime.ticks_us(), self.period)
        self.closedLoop = closedLoop
        
        
    def run(self):
        ''' @brief      Runs the motor task.
            @details    Updates the motor duty cycle from motorShare.
        '''
        if utime.ticks_diff(utime.ticks_us(), self.nextTime) >= 0:
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            self.closedLoop.update()