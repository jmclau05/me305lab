''' @file DRV8847.py
    @brief                      Contains Motor and DRV8847 motor driver classes.
    @details                    Contains classes for motors and the DRV8847 
                                motor driver from TI.
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       October 31, 2021
'''

import pyb
import utime

class DRV8847:
    ''' @brief      A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or more objects of the
                    Motor class which can be used to perform motor
                    control.
                                          
                    Refer to the DRV8847 datasheet here:
                    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
      
    def __init__ (self, faultShare):
        ''' @brief      Initializes and returns a DRV8847 object.
            @param      faultShare      Share to store whether a fault has been triggered.
            @details    This driver stores the pins and timers associated with
                        the motors. It has methods for enabling and disabling 
                        the motor driver, and for initializing the motors.
                        It sets up an interrupt that will stop the motors
                        and disable the driver when a fault is detected.
        '''          
        timer3 = pyb.Timer(3, freq = 20000)
        
        pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
        pinB5 = pyb.Pin(pyb.Pin.cpu.B5)
        pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
        pinB1 = pyb.Pin(pyb.Pin.cpu.B1)
        
        self.pinFault = pyb.Pin(pyb.Pin.cpu.B2)
        self.faultInterrupt = pyb.ExtInt(self.pinFault, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
        
        self.pinSleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        
        self.t3ch1 = timer3.channel(1, pyb.Timer.PWM, pin=pinB4)
        self.t3ch2 = timer3.channel(2, pyb.Timer.PWM, pin=pinB5)
        self.t3ch3 = timer3.channel(3, pyb.Timer.PWM, pin=pinB0)
        self.t3ch4 = timer3.channel(4, pyb.Timer.PWM, pin=pinB1)
        
        self.faultShare = faultShare
  
    def enable (self):
        ''' @brief      Brings the DRV8847 out of sleep mode.
            @details    Will check if a fault has been stored recently and not
                        cleared yet. If there is no fault, will attempt to
                        enable the motor driver again by setting the nSLEEP pin
                        high. Since the motor driver sometimes shows a fault
                        on the nFAULT pin immediately after turning the nSLEEP
                        pin on, the fault interrupt is disabled for 25
                        microseconds while enabling.
        '''
        if not self.faultShare.read(): # Will not enable if there is a fault
            self.faultInterrupt.disable()
            self.pinSleep.high()
            utime.sleep_us(25)
            self.faultInterrupt.enable()
        else:
            print("Must clear fault to enable")
    
    def disable (self):
        ''' @brief      Puts the DRV8847 in sleep mode.
            @details    Sets the nSLEEP pin on the DRV8847 low, so that the
                        motor driver is disabled. In this state no power can 
                        be applied to the motors so they will not spin.
        '''
        self.pinSleep.low()

    def fault_cb (self, IRQ_src):
        ''' @brief      Callback function to run on fault condition.
            @param      IRQ_src The source of the interrupt request.
        '''
        print("fault detected")
        self.t3ch1.pulse_width_percent(0)
        self.t3ch2.pulse_width_percent(0)
        self.t3ch3.pulse_width_percent(0)
        self.t3ch4.pulse_width_percent(0)
        self.disable()
        self.faultShare.write(True)
        
    def motor (self, motorID):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @details    Initializes a motor object. For the motor ID (1 or 2)
                        the timer channels associated with out board are used.
                        These are timer 3, channel 1 (pin B4) and 2 (pin B5)
                        for motor 1 and timer 3, channel 3 (pin B0) and 4 (pin B1)
                        for motor 2.
            @param      motorID     The ID of the motor to get (1 or 2).
            @return     An object of class Motor.
        '''
        if motorID == 1:
            return Motor(self.t3ch1, self.t3ch2)
        elif motorID == 2:
            return Motor(self.t3ch3, self.t3ch4)
        else:
            raise ValueError("Invalid Motor ID. Motor ID should be 1 or 2.")

        
class Motor:
    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''
        
    def __init__ (self, ch1, ch2):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @details    Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method
                        DRV8847.motor().
            @param      ch1     First timer channel to use for the motor.
            @param      ch2     Second timer channel to use for the motor.
        '''
        self.ch1 = ch1
        self.ch2 = ch2
                    
    def set_duty (self, duty):
        ''' @brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty    A number between -100 and 100 (percent) holding
                                the duty cycle of the PWM signal sent to the motor.
        '''
        if duty < 0.0 and duty >= -100.0:
            self.ch1.pulse_width_percent(0.0)
            self.ch2.pulse_width_percent(-1.0*duty)
        elif duty == 0.0:
            self.ch1.pulse_width_percent(0.0)
            self.ch2.pulse_width_percent(0.0)
        elif duty > 0.0 and duty <= 100.0:
            self.ch1.pulse_width_percent(duty)
            self.ch2.pulse_width_percent(0.0)
        else:
            raise ValueError("Invalid duty cycle {}. Duty cycle should be between -100 and 100".format(duty))
    
    
if __name__ =='__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    
    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.
    motor_drv     = DRV8847()
    motor_1       = motor_drv.motor(1)
    motor_2       = motor_drv.motor(2)
    
    # Enable the motor driver
    motor_drv.enable()
    
    # Set the duty cycle of the first motor to 40 percent and the duty cycle of
    # the second motor to 60 percent
    
    motor_1.set_duty(40)
    motor_2.set_duty(60) 

    