''' @file                       task_motor.py
    @brief                      A task for setting a motor's duty cycle.
    @details                    Contains the class Task_Motor.
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       October 31, 2021
'''

import utime

class Task_Motor:
    ''' @brief      A task for setting a motor's duty cycle.
        @details    Reads a motor's duty cycle from a share and sets it. 
                    Attempts to enable the motor each run (will fail if
                    fault exists).
                    
    '''
    
    def __init__(self, motorID, motorShare, faultShare, period, motor_drv):
        ''' @brief      Initializes a motor task.
            @details    Sets up a motor to read its duty cycle from a share and
                        set it, periodically.
            @param      motorID     The ID of the motor to use (1 or 2).
            @param      motorShare  The share holding the duty cycle values to be set.
            @param      period      The period, in microseconds, to run this task.
            @param      motor_drv   The motor driver object.
        '''
        self.motorID = motorID
        self.motorShare = motorShare
        self.faultShare = faultShare
        self.motor_drv = motor_drv
        self.relevantMotor = self.motor_drv.motor(motorID)
        self.period = period
        self.nextTime = utime.ticks_add(utime.ticks_us(), self.period)

        
    def run(self):
        ''' @brief      Runs the motor task.
            @details    Updates the motor duty cycle from motorShare, and
                        attempts to enable the motor driver.
        '''
        if utime.ticks_diff(utime.ticks_us(), self.nextTime) >= 0:
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            self.relevantMotor.set_duty(self.motorShare.read())
            if not self.faultShare.read():
                self.motor_drv.enable()