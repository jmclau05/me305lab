''' @file                       encoder.py
    @brief                      A driver for reading from Quadrature Encoders
    @details                    Includes the class Encoder.
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       October 31, 2021
'''

import pyb

class Encoder:
    ''' @brief                  Interface with quadrature encoders
        @details                This class is a driver for quadrature encoders.
                                It uses the timer functions of the nucleo board.
                                It provides functions for getting position and
                                delta while being safe from overflow or underflow
                                errors, and allows the position of the encoder
                                to be set by the user.
    '''
        
    def __init__(self, timerID, pin1, pin2):
        ''' @brief      Constructs an encoder object
            @details    The Encoder object stores position and delta values and
                        provides methods to get position or delta and to set
                        position. The hardware timer is set up in this constructor.
            @param      timerID     Number of timer to use.
            @param      pin1        First encoder pin.
            @param      pin2        Second encoder pin.
        '''
        self.period = 2 ** 16 - 1
        self.timer = pyb.Timer(timerID,period=(self.period),prescaler=0)
        self.channel_1 = self.timer.channel(1,mode=pyb.Timer.ENC_AB,pin=pin1)
        self.channel_2 = self.timer.channel(2,mode=pyb.Timer.ENC_AB,pin=pin2)
        self.position = 0
        self.delta = 0
        self.lastTick = 0
        self.currentTick = 0
        
    def update(self):
        ''' @brief      Updates encoder position and delta
            @details    Updates variables which store position values and 
                        change in position (delta) values. Compensates for
                        overflow and underflow.
        '''
        self.lastTick = self.currentTick
        self.currentTick = self.timer.counter()
        self.delta = self.currentTick - self.lastTick
        
        if self.delta >= self.period / 2:
            self.delta -= self.period
        elif self.delta <= -self.period / 2:
            self.delta += self.period
        
        self.position += self.delta
            
    def get_position(self):
        ''' @brief              Returns encoder position
            @details            Returns the encoder position, found by update().
                                This is compensated for overflow/underflow.
                                Call update() before to ensure most recent 
                                position.
            @return             The position of the encoder shaft.
        '''
        return self.position
    
    def set_position(self, desiredPosition):
        ''' @brief              Sets encoder position
            @details            Sets the encoder position to a user inputted
                                value.
            @param              desiredPosition     The new position of the
                                                    encoder shaft.
        '''
        self.position = desiredPosition
        
    def get_delta(self):
        ''' @brief              Returns encoder delta
            @details            Returns the encoder delta, found by update().
                                This is compensated for overflow/underflow.
                                Call update() before to ensure most recent 
                                delta. delta is the difference in ticks 
                                between the two most recent updates.
            @return             The change in position of the encoder shaft
                                between the two most recent updates.
        '''
        return self.delta