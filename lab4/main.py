''' @file                       main.py
    @brief                      Main file for a encoder display program.
    @details                    Runs an encoder, motor, controller and user task which
                                allow the position and delta of an encoder
                                to be read and collected, and the encoder
                                to be zeroed. The duty cycle of each motor can
                                be set manually. A step response test can be run
                                on either motor using the encoder for feedback
                                control. Holds Shares and Queues for 
                                data exchange between tasks. Task diagram is
                                below.
                                \image html Lab4taskdiagram.jpg
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       October 31, 2021
'''
import task_encoder
import task_user
import task_motor
import task_controller
import shares
import DRV8847
import closedloop

ENCODER_PERIOD = 1000       # us = 1000 Hz
USER_PERIOD = 10000         # us = 100 Hz
COLLECT_PERIOD = 1000000    # us = 1 Hz
MOTOR_PERIOD = 1000         # us = 1000 Hz
CONTROLLER_PERIOD = 1000         # us = 1000 Hz

if __name__ == '__main__':
    zeroShare1 = shares.Share(False)
    zeroShare2 = shares.Share(False)
    
    dataShare1 = shares.Share()
    dataShare2 = shares.Share()
    
    faultShare = shares.Share(False)
    
    motorShare1 = shares.Share(0.0)
    motorShare2 = shares.Share(0.0)
    
    omegaMeasuredShare1 = shares.Share(0.0)
    omegaDesiredShare1 = shares.Share(0.0)
    
    omegaMeasuredShare2 = shares.Share(0.0)
    omegaDesiredShare2 = shares.Share(0.0)
    
    srQueue = shares.Queue()
        
    dataQueue = shares.Queue()
    
    motor_drv = DRV8847.DRV8847(faultShare)
    motor_drv.enable()
    
    closedLoop1 = closedloop.ClosedLoop(1, omegaMeasuredShare1, 
                                        omegaDesiredShare1, motorShare1)
    closedLoop2 = closedloop.ClosedLoop(1, omegaMeasuredShare2, 
                                        omegaDesiredShare2, motorShare2)
    
    motor1 = task_motor.Task_Motor(1, motorShare1, MOTOR_PERIOD, motor_drv)
    motor2 = task_motor.Task_Motor(2, motorShare2, MOTOR_PERIOD, motor_drv)
    
    encoder1 = task_encoder.Task_Encoder(ENCODER_PERIOD, zeroShare1, 
                                         dataShare1, omegaMeasuredShare1, 1)
    encoder2 = task_encoder.Task_Encoder(ENCODER_PERIOD, zeroShare2, 
                                         dataShare2, omegaMeasuredShare2, 2)
    
    controller1 = task_controller.Task_Controller(CONTROLLER_PERIOD, closedLoop1)
    controller2 = task_controller.Task_Controller(CONTROLLER_PERIOD, closedLoop2)
    
    
    
    user = task_user.Task_User(USER_PERIOD, COLLECT_PERIOD, 
                               zeroShare1, zeroShare2,
                               dataShare1, dataShare2, 
                               faultShare,
                               motorShare1, motorShare2,
                               omegaDesiredShare1, omegaDesiredShare2,
                               omegaMeasuredShare1, omegaMeasuredShare2,
                               closedLoop1, closedLoop2,
                               dataQueue, srQueue)    


    user.list_commands()
    
    while True:
        try:
            controller1.run()
            controller2.run()
            encoder1.run()
            encoder2.run()
            motor1.run()
            motor2.run()
            user.run()
        except KeyboardInterrupt:
            motor_drv.disable()
            print("exited")
            break
