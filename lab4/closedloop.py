''' @file                       closedloop.py
    @brief                      A closed loop controller.
    @details                    Contains the class ClosedLoop
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       October 26, 2021
'''

class ClosedLoop:
    ''' @brief      A class for controlling the velocity of a motor with a closed loop.
        @details    Takes data from shares for the measured and desired 
                    velocity. Outputs data to a share for the motor duty cycle.
                    Stores a proportional gain constant. The available methods
                    will update the motor's duty cycle, set and return the 
                    current proportional gain constant, and enable and
                    disable the controller.
    '''
    def __init__(self, Kp, omegaMeasuredShare, omegaDesiredShare, motorShare):
        ''' @brief      Constructs a closed loop controller.
            @details    The closed loop controller object takes data from shares
                        for the measured and desired velocity. It outputs data
                        to a share for the motor duty cycle. It stores a
                        proportional gain constant.
            @param      Kp                      Initial proportional gain constant (%%duty/(rad/s))
            @param      omegaDesiredShare       Share to store desired motor velocity (rad/s).
            @param      omegaMeasuredShare      Share to store measured motor velocity (rad/s).
            @param      motorShare              The share holding the duty cycle values to be set for motor.
        '''
        self.Kp = Kp
        self.omegaMeasuredShare = omegaMeasuredShare
        self.omegaDesiredShare = omegaDesiredShare
        self.motorShare = motorShare
        self.enabled = False
    
    def update(self):
        ''' @brief      Updates the control loop.
            @details    Uses the proportional gain constant to set the motor's duty cycle.
                        The error between the desired velocity and the meausured
                        velocity is computed, then multiplied by the proportional gain.
                        A saturation limit is then applied to this since the motor
                        cannot run above 100% duty cycle.
        '''
        error = self.omegaDesiredShare.read() - self.omegaMeasuredShare.read()
        if self.enabled:
            if error * self.Kp >= 0:
                duty = min(error * self.Kp, 100)
            else:
                duty = max(error * self.Kp, -100)
            self.motorShare.write(duty)
    
    def get_Kp(self):
        ''' @brief      Returns the proportional gain constant.
            @details    Returns the current proportional gain for the closed
                        loop controller. It has units of %%duty/(rad/s).
            @return     Proportional gain constant Kp
        '''
        return self.Kp
    
    def set_Kp(self, Kp):
        ''' @brief      Sets the proportional gain constant.
            @details    Sets the current proportional gain for the closed
                        loop controller. It has units of %%duty/(rad/s).
            @param      Kp  Proportional gain constant (%%duty/(rad/s))
        '''
        self.Kp = Kp
        
    def enable(self):
        ''' @brief      Enables the closed loop controller.
            @details    Makes it so that the closed loop controller sets the
                        duty cycle for the motor. Don't try to set the duty
                        cycle manually when the controller is enabled.
        '''
        self.enabled = True
    
    def disable(self):
        ''' @brief      Disables the closed loop controller.
            @details    Makes it so that the closed loop controller will not
                        write to motorShare. This allows motor duty cycle to
                        be set by something else, like our manual input.
        '''
        self.enabled = False
        self.motorShare.write(0)
    