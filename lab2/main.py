''' @file                       main.py
    @brief                      Main file for a encoder display program.
    @details                    Runs an encoder task and user task which
                                allow the position and delta of an encoder
                                to be read and collected, and the encoder
                                to be zeroed. Holds Shares and Queues for 
                                data exchange between tasks. Task diagram is
                                below.
                                \image html Lab2taskdiagram.jpg
    @author                     Tori Bornino
    @author                     Jackson McLaughlin
    @date                       October 16, 2021
'''
import task_encoder
import task_user
import shares


ENCODER_PERIOD = 1000       # us = 1000 Hz
USER_PERIOD = 10000         # us = 100 Hz
COLLECT_PERIOD = 1000000    # us = 1 Hz

if __name__ == '__main__':
    zeroShare = shares.Share(False)
    dataShare = shares.Share()
    dataQueue = shares.Queue()
    
    encoder1 = task_encoder.Task_Encoder2(ENCODER_PERIOD, zeroShare, dataShare)
    user = task_user.Task_User2(USER_PERIOD, COLLECT_PERIOD, zeroShare, dataShare, dataQueue)
        
    user.list_commands()
    while True:
        try:
            encoder1.run()
            user.run()
        except KeyboardInterrupt:
            print("exited")
            break
