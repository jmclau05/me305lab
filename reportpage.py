''' @file reportpage.py
    @brief Describes our term project
    
    \page final Final Ball Balancing Platform
    
    @section sec_report Term Project
    @section sec_goals Goals and Hardware
    For our term project, our goal was to balance a ball on a platform using
    a kit provided by the class. This kit was equipped with 2 motors which
    tilted the x and y axes of the platform, a resistive touch panel which
    was used to sense the position of the ball on the platform, and an IMU
    attached to the platform that was used to sense the platform orientation.

    @section sec_fw_design Firmware Design
    Control of the platform was implemented with a state space controller.
    The system dynamics were modeled with some key simplifying assumptions:
        1.  The x and y axes of the plate can be treated independently.
            A separate state feedback controller will be used for each axis.
        2.  The ball stays in contact with the plate and rolls without slip.
        3.  The inertia of the linkage which connects the motor to the plate
            can be ignored.
    This model was detailed in the 
    @ref term "Homework 2 and 3" page.
    The state of the system was represented by the position and velocity of
    the ball, and the angular position and angular velocity of the platform.
    The system was to be controlled using full state feedback.
    
    @section sec_code_organization Code Organization
    Our code consists of five tasks to balance the ball:
    \image html FinalTaskDiagram.jpg "Ball Balancing Platform Task Diagram"
        1.  imu_task: A task that reads platform angle and angular velocity 
            from the imu.
        2.  panel_task: A task that reads the ball's position on the 
            resistive touch panel, then used an alpha/beta filter to compute 
            the ball's position and velocity on the platform.
        3.  controller_task: A task that reads the ball's location and 
            platform's angle from the imu and panel tasks then computes and 
            sets the motors' duty cycles.
        4.  data_collect_task: A task that reads the ball's location and 
            platform's angle from the imu and panel tasks as well as the 
            motors' duty cycles from the controller task, then prints them
            to the console.
        5.  user_task: a task that lets the user interact with the ball 
            balancing platform by beginning and ending balancing the ball 
            and beginning and ending recording data to the console.
            \image html FinaltaskuserFSM.jpg "User Task FSM"
    
    @section sec_resp_plots System Response Plots
    We did not succeed in tuning the system such that the ball balanced on 
    the platform; however, our platform responds as one would expect to the 
    ball's position. As shown in the X response plot, when the ball goes in
    the positive x direction, the plate rotates about the negative y axis as 
    we would expect.
    \image html FinalTuneX.jpg "X Response Plot"
    As shown in the Y response plot, when the ball goes in
    the positive Y direction, the plate rotates about the positive x axis as 
    we would expect.
    \image html FinalTuneY.jpg "Y Response Plot"

    @section vid Video
    @htmlonly <iframe width="560" height="315" src="https://www.youtube.com/embed/sV7zGjEcGug" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> @endhtmlonly
'''