'''@file                mainpage.py
   @brief               Main page for documentation site.
   @details             This is the main page for the doxygen site. It contains links to 
                        the code for each lab and some information about each one. 

   @mainpage

   @section sec_intro   Introduction
                        This is the documentation site for Tori Bornino and Jackson Mclaughlin's
                        code used in ME 305 - Mechatronics, taught in Fall 2021 by
                        Charlie Revfem. Source code is available at https://bitbucket.org/jmclau05/me305lab/

   @section sec_fib     Lab 0: Fibonacci Sequence
                        First lab, 
                        Please see fibonacci.py for details.

   @section sec_light   Lab 1: Light Show
                        First Nucleo lab, uses a Finite State Machine
                        to implement 3 LED blinking patterns which can be cycled
                        by pressing a button. Patterns are square, sine, and
                        sawtooth waves.
                        Please see lightshow.py for details.
                        A video of the code running can be seen below:

                        @htmlonly
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/3SUByQqFwUY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        @endhtmlonly

   @section sec_enc     Lab 2: Encoder Display
                        In this lab we created an encoder driver class
                        @ref encoder.Encoder "Encoder", a task class 
                        @ref task_encoder.Task_Encoder2 "Task_Encoder2" for updating the encoder and a task class
                        @ref task_user.Task_User2 "Task_User2" for taking user input. 
                        These are used to create a program which lets the user
			               print current readings for position and delta, as well as collect them over a period of time.
                        We learned to pass data around using the shares.Share and shares.Queue classes that were
                        provided to us.
                        A task diagram is available in the lab2/main.py documentation.
                        **Note:** The Task_User and Task_Encoder classes have been updated in labs 3 and 4.
                        The above links go to the lab 2 versions, which have the suffix -2 to distinguish them.

    @section sec_mot    Lab 3: Motor test
                        In this lab we tested the motors and the motor driver for the first time. We made
                        a class which represented the motor driver, 
                        @ref DRV8847.DRV8847 "DRV8847"
                        and was able to return objects of the
                        @ref DRV8847.Motor "Motor"
                        class. These motor objects could have a duty cycle set using PWM. We made a task 
                        @ref task_motor.Task_Motor "Task_Motor"
                        to update the duty cycles of the motor by reading from a 
                        @ref shares.Share "Share", 
                        and extended our user interface to allow setting the duty cycle of either motor and collecting data from either encoder.
                        We updated our user task and encoder task to include these new features. The lab 3 versions are
                        @ref task_user.Task_User3 "Task_User3" and 
                        @ref task_encoder.Task_Encoder3 "Task_Encoder3".
                        See lab3/main.py for a task diagram.
    
    @section sec_clc    Lab 4: Closed Loop Control
                        In this lab we used the encoder reading and motor driving capabilities from Lab 2 and 3 to
                        implement a proportional feedback controller in the class
                        @ref closedloop.ClosedLoop "ClosedLoop".
                        It was updated periodically by the
                        @ref task_controller.Task_Controller "Task_Controller"
                        task. We extended the user interface to allow running step response tests on the controller. 
                        The results of tuning the controller can be seen on the
                        @ref tuning "Closed Loop Control" page.
                        We updated our user task and encoder task to include these new features. The lab 4 (latest) versions are
                        @ref task_user.Task_User "Task_User" and 
                        @ref task_encoder.Task_Encoder "Task_Encoder" (no suffixes for latest version).
                        See lab4/main.py for a task diagram.

   @section sec_imu     Lab 5: BNO055 IMU Driver
                        In this lab we implemented a driver class
                        @ref BNO055.BNO055 "BNO055" 
                        for the BNO055 IMU from Bosch. It can set up the IMU,
                        and read the euler angles and angular velocities.
                        In the same file BNO055.py, there is also a simple
                        calibration and test program. This can load calibration
                        coefficients from a file, or calibrate the IMU and
                        save the calibration coefficients for later use.

   @section sec_hw2	Homework 2 & 3: Balancing platform model and simulation
			For this homework assignment we developed a model for the balancing platform and the resulting 			
			simulation. Find the model and simulation at: 		 
			@ref term "Homework 2 and 3"

   @section Final	Final Project: Balancing Ball Platform
			For the final lab we combined all of the code developed throughout the quarter into one program 
			that should balance the ball on the platform. The final implementation is further detailed in the 
			@ref final "Final Ball Balancing Platform" page.

   @author              Tori Bornino
   @author              Jackson McLaughlin
   @author              Kendall Chappell		

   @date                December 10, 2021
'''