''' @file                termpage.py
    @brief               Page describing the closed loop system and its tuning.
    
    @page term  Homework 2 and 3
    @htmlinclude termproject.html
'''  